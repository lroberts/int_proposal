all: motivations_impact.pdf


motivations_impact.pdf: motivations_impact.tex
	pdflatex motivations_impact.tex
	bibtex motivations_impact
	pdflatex motivations_impact.tex
	pdflatex motivations_impact.tex

clean:
	rm -f motivations_impact.aux
	rm -f motivations_impact.bbl
	rm -f motivations_impact.blg
	rm -f motivations_impact.log
	rm -f motivations_impact.out

veryclean: clean
	rm -f motivations_impact.pdf
